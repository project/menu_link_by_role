Role Based Custom Menu Link Drupal 9 Module
===========================================

This module extends the functionality of Drupal's menu system by allowing administrators to define different menu links for different user roles. It provides a form alteration hook, a custom submit handler, and a preprocess function for menu rendering.

Requirements
------------
- Drupal 9.x

Installation
------------
1. Download the "menu_link_by_role" module.
2. Place the module directory in the "modules" folder of your Drupal installation.
3. Enable the module through the Drupal administrative interface or using Drush command: `drush en menu_link_by_role`

Usage
-----
1. After installing the module, navigate to the administrative interface.
2. Go to the "Structure" > "Menus" and select the desired menu.
3. Create or edit a menu link.
4. You will find additional fields for each user role.
5. Enter the custom link for each role as desired.
6. Save the menu link.
7. The custom links will be associated with the corresponding user roles and used when rendering the menu.

Customization
-------------
- You can customize the behavior of the module by modifying the provided functions in the "menu_link_by_role.module" file.
- The "menu_link_by_role_form_alter" function alters the menu link content form and adds custom link fields for each role.
- The "menu_link_by_role_form_submit" function handles the form submission and saves the custom link values in the database.
- The "menu_link_by_role_preprocess_menu" function preprocesses the menu rendering and replaces the original links with custom links based on the user's role.
- The "menu_link_by_role_loadCustomLink" function fetches the custom link value from the database based on the menu link and role.

Contributing
------------
Contributions are welcome! If you find any issues or have suggestions for improvement, please submit an issue or pull request on the module's GitHub repository.

License
-------
This module is licensed under the [GNU General Public License version 2 or later](https://www.gnu.org/licenses/gpl-2.0.html).

Author
------
Created by Yashaswini

Current maintainers
--------------------
 * Vishak Raman - https://www.drupal.org/u/vishakraman
 * Yashaswini B - https://www.drupal.org/u/yashaswini
