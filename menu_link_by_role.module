<?php

/**
 * @file
 * Module file for menu_link_by_role.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_form_alter().
 */
function menu_link_by_role_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  // Check the form ID or route.
  if ($form_id == 'menu_link_content_menu_link_content_form' || $form_id == 'menu_link_content_menu_link_content_edit_form') {

    // Load all roles.
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();

    foreach ($roles as $role) {
      $role_id = $role->id();

      // Add a custom link field for each role.
      $form['link_' . $role_id] = [
        '#type' => 'url',
        '#title' => $role->label() . ' link',
      // Add your default value here.
        '#default_value' => '',
      ];

      // Check if we're editing an existing menu link.
      if ($form_state->getFormObject()->getEntity()->isNew() === FALSE) {
        // Fetch the stored custom link and set as the default value if it exists.
        $plugin_id = $form_state->getFormObject()->getEntity()->getPluginId();
        $custom_link = menu_link_by_role_loadCustomLink($plugin_id, $role_id);
        if (!empty($custom_link)) {
          $form['link_' . $role_id]['#default_value'] = $custom_link;
          // kint($custom_link);
        }
      }
    }
    foreach (array_keys($form['actions']) as $action) {
      if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
        $form['actions'][$action]['#submit'][] = 'menu_link_by_role_form_submit';
      }
    }
  }
}

/**
 * Implements hook_preprocess_menu().
 */
function menu_link_by_role_preprocess_menu(&$variables) {
  $user_roles = \Drupal::currentUser()->getRoles();
  foreach ($variables['items'] as $key => $item) {
    // Load your custom links.
    // Check each user role and set the appropriate link.
    foreach ($user_roles as $user_role) {
      // Assume loadCustomLink is a function that fetches the custom links based on the menu link and role.
      $custom_link = menu_link_by_role_loadCustomLink($item['original_link']->getPluginId(), $user_role);
      if (!empty($custom_link)) {
        $variables['items'][$key]['url'] = Url::fromUri($custom_link);
      }
    }
  }
}

/**
 *
 */
function menu_link_by_role_form_submit(array $form, FormStateInterface $form_state) {
  $database = \Drupal::database();
  $menu_link_plugin_id = $form_state->getFormObject()->getEntity()->getPluginId();

  // Load all roles.
  $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();

  foreach ($roles as $role) {
    $role_id = $role->id();
    $custom_link = $form_state->getValue('link_' . $role_id);

    // Check if there's already a custom link for this menu link and role.
    $existing = $database->select('menu_role_link', 'menu')
      ->fields('menu', ['menu_link_plugin_id'])
      ->condition('menu_link_plugin_id', $menu_link_plugin_id)
      ->condition('role', $role_id)
      ->execute()
      ->fetchField();

    // If there's an existing row, update it. Otherwise, insert a new row.
    if ($existing) {
      $database->update('menu_role_link')
        ->fields([
          'custom_link' => $custom_link,
        ])
        ->condition('menu_link_plugin_id', $menu_link_plugin_id)
        ->condition('role', $role_id)
        ->execute();
    }
    else {
      $database->insert('menu_role_link')
        ->fields([
          'menu_link_plugin_id' => $menu_link_plugin_id,
          'role' => $role_id,
          'custom_link' => $custom_link,
        ])
        ->execute();
    }
  }
}

/**
 *
 */
function menu_link_by_role_loadCustomLink($menu_link_plugin_id, $user_role) {
  // Fetch the custom link from your database.

  $connection = \Drupal::database();
  $query = $connection->select('menu_role_link', 'menu')
    ->fields('menu', ['custom_link'])
    ->condition('menu_link_plugin_id', $menu_link_plugin_id)
    ->condition('role', $user_role);
  $result = $query->execute()->fetchField();

  return $result;
}
